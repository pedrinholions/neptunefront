import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {LayoutComponent} from './components/common/layout/layout.component';
import {ActivatedRoute, ActivatedRouteSnapshot, RouterModule} from '@angular/router';
import { ClientComponent } from './components/client/client.component';
import {AppRouting} from './app.routing';
import {HttpClientModule} from '@angular/common/http';
import { ClientFormComponent } from './components/client/client-form/client-form.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
      LayoutComponent,
      ClientComponent,
      ClientFormComponent,
  ],
  imports: [
      BrowserModule,
      AppRouting,
      AngularFontAwesomeModule,
      AngularFontAwesomeModule,
      HttpClientModule,
      FormsModule,
      ReactiveFormsModule,
      RouterModule,
  ],
    exports: [
        RouterModule
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
