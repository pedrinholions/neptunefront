import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class ClientService {

    readonly API = environment.api;

    constructor(
        private http: HttpClient
    ) {
    }

    getAllClients(): Observable<any> {
        return this.http.get(this.API + 'clients');
    }

    store(body): Observable<any> {
        return this.http.post(this.API + 'clients', body);
    }

    show(client_id): Observable<any> {
        return this.http.get(this.API + 'clients/' + client_id);
    }

    update(data): Observable<any> {
        return this.http.put(this.API + 'clients/' + data.id, {params: data});
    }

    destroy(client_id): Observable<any> {
        return this.http.delete(this.API + 'clients/' + client_id);
    }
}
