import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  readonly API = environment.api;

  constructor(
      private http: HttpClient
  ) { }

  getStates(): Observable<any> {
    return this.http.get(this.API + 'states');
  }

  getCitiesByState(state_id): Observable<any> {
    return this.http.get(`${this.API}cities/${state_id}`);
  }

  findZipCode(zipcode): Observable<any> {
    return this.http.get(`https://viacep.com.br/ws/${zipcode}/json/`);
  }

}
