import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {LayoutComponent} from './components/common/layout/layout.component';
import {ClientComponent} from './components/client/client.component';
import {ClientFormComponent} from './components/client/client-form/client-form.component';

export const routes: Routes = [
    {
        path: '',
        redirectTo: 'dashboard',
        pathMatch: 'full',
    },
    {
        path: 'dashboard',
        component: LayoutComponent,
    },
    {
        path: 'clientes',
        component: LayoutComponent,
        children: [
            {
                path: '',
                component: ClientComponent
            },
            {
                path: 'novo',
                component: ClientFormComponent
            },
            {
                path: ':id/editar',
                component: ClientFormComponent
            }
        ],
    }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRouting {
}