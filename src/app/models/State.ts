export class State {
    id: number = null;
    name: string = null;
    uf: string = null;
    cities: any[];
}