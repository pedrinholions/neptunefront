import {City} from './City';
import {State} from './State';

export class Address {
    id: number = null;
    street: string = '';
    neightborhood: string = null;
    number: string = null;
    complement: string = null;
    zipcode: string = null;
    city: City = new City();
    state: State = new State();
    country: string = null;
    note: string = null;
}