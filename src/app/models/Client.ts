import {Address} from './Address';

export class Client {
    id: number = null;
    address_id: number = null;
    name : string = null;
    cnpj: string = null;
    email: string = null;
    telephone: string = null;
    address: Address = new Address();
}