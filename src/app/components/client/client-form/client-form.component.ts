import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AddressService} from '../../../services/address.service';
import {State} from '../../../models/State';
import {City} from '../../../models/City';
import {ClientService} from '../../../services/client.service';

import swal from 'sweetalert';
import 'jquery-mask-plugin';
import {ActivatedRoute, ActivatedRouteSnapshot, Router} from '@angular/router';
import {Client} from '../../../models/Client';

declare var $:any;

@Component({
    selector: 'app-client-form',
    templateUrl: './client-form.component.html',
    styleUrls: ['./client-form.component.scss']
})
export class ClientFormComponent implements OnInit {

    formClient: FormGroup;
    states: State[];
    cities: City[];

    action: string;
    client: Client = new Client;

    btnDisable: boolean = false;

    constructor(
        private formBuilder: FormBuilder,
        private addressService: AddressService,
        private clienteService: ClientService,
        private router: Router,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.verifyAction();

        this.addressService.getStates()
            .subscribe(res => {
                console.log(res);
                this.states = res.data;
            });

        if (this.action == 'new') {
            this.buildForm();
            this.onStateChanges();
        } else {
            let service_id = this.route.snapshot.params['id'];
            this.clienteService.show(service_id)
                .subscribe(res => {
                    this.client = res.data;
                    console.log(this.client);
                    this.buildForm();
                    this.onStateChanges();
                });
        }

        $( () => {
            $("#telephone").mask("(11) 0000-0000");
            $("#cnpj").mask("00.000.000/0000-00");
            $("#zipcode").mask("00000-000");
        });
    }

    onSubmit() {
        if (this.formClient.valid) {
            if (this.action == 'new') {
                this.clienteService.store(this.formClient.value)
                    .subscribe(res => {
                        if (res.status === 200) {
                            swal({
                                title: "Bom trabalho!",
                                text: "Cliente adicionado com sucesso!",
                                icon: "success",
                            });
                        }

                        this.router.navigate(['/clientes']);
                    });
            } else {
                console.log(this.formClient.value);
                this.clienteService.update(this.formClient.value)
                    .subscribe(res => {
                        console.log(res);
                    });
            }
        } else {

        }
    }

    buildForm() {
        this.formClient = this.formBuilder.group({
            // cliente
            id: [this.client.id || null, Validators.required],
            name: [this.client.name || null, Validators.required],
            cnpj: [this.client.cnpj || null, Validators.required],
            email: [this.client.email || null, [Validators.required, Validators.email]],
            telephone: [this.client.telephone || null, Validators.required],
            // address
            street: [this.client.address.street || null],
            neighborhood: [this.client.address.neightborhood || null],
            number: [this.client.address.number || null],
            complement: [this.client.address.complement || null],
            zipcode: [this.client.address.zipcode || null],
            city_id: [this.client.address.city.id || null, Validators.required],
            state_id: [this.client.address.state.id || null, Validators.required],
            country: [this.client.address.country || null],
            note: [this.client.address.note || null],
        });
    }


    onStateChanges(): void {
        this.formClient.get('state').valueChanges.subscribe(val => {
            this.addressService.getCitiesByState(val)
                .subscribe(res => {
                    this.cities = res.data;
                });
        });
    }

    verifyAction() {
        if (this.route.snapshot.params['id'] !== undefined) return this.action = 'edit';
        return this.action = 'new';
    }


    verifiedValidTouched(formName, inputName) {
        return this[formName].get(inputName).invalid && this[formName].get(inputName).touched;
    }

    applyCSSInvalid(formName, inputName) {
        return {
            'is-invalid': this.verifiedValidTouched(formName, inputName),
        };
    }

    findZipCode() {
        let zipcode = this.formClient.get('zipcode').value;
        zipcode = zipcode.replace('-', '');

        this.addressService.findZipCode(zipcode)
            .subscribe(res => {
                console.log(res);
                this.formClient.controls['street'].setValue(res.logradouro);
                this.formClient.controls['neighborhood'].setValue(res.bairro);
                this.formClient.controls['state_id'].setValue(res.uf);
            });
    }


}
