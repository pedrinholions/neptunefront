import {Component, OnInit} from '@angular/core';
import {ClientService} from '../../services/client.service';
import swal from 'sweetalert';

@Component({
    selector: 'app-client',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {

    clientes: any[];

    constructor(private service: ClientService) {
    }

    ngOnInit() {
        this.service.getAllClients().subscribe(res => {
            console.log(res);
            if (res.status === 200) this.clientes = res.data;
        });
    }

    destroy(client_id, index) {
        this.service.destroy(client_id)
            .subscribe(res => {
                swal({
                    title: "Bom trabalho!",
                    text: "Cliente removido com sucesso!",
                    icon: "success",
                });

                this.clientes.splice(index, 1);
            });
    }

    newClientModal() {
        return;
    }

}
